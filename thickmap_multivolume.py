# module add psi-python36/4.4.0; module add gcc/7.3.0

import numpy as np
import time
import os.path
from _thickmapopt import *
from thickmap import *

def loadDistanceRidgeAndSeparate(DR_big_source,workdir,Filename_template,dims_orig,dims_target,is_big_endian):
    """
    This function loads the BIG distance ridge as memory map
    and launches the DR-separator which creates single files
    with distance ridges from the subvolumes
    """
    print("...loading distance ridge and seperating into subvolumes...")
    
    absFilename_template = os.path.join(workdir,Filename_template)
    
#    DR_big_array = np.memmap(DR_big_source,dtype=np.float32,mode='readonly',shape=(dims_orig[2],dims_orig[1],dims_orig[0]),order='C')
    DR_big_array = np.memmap(DR_big_source,dtype=np.float32,mode='readonly',shape=(dims_orig[0],dims_orig[1],dims_orig[2]),order='C') # change [lena]
    subdivideridge(DR_big_array, absFilename_template, dims_orig, dims_target, is_big_endian)


def loadDistanceRidgeSubvolume(absFilename_template,number):
    """
    Load a binary file with distance ridge values and their coordinates
    that are sorted consecutevly in the binary file as vectors:
    [Radius, X, Y, Z]_1, [Radius, X, Y, Z]_2, ...    
    """
    print("...loading ridge points from subvolume...")
    
    temp_subvolume_path = absFilename_template + str(number) + ".dat"
    
    dt = np.dtype([('R', '=f4'), ('x', '=i4'), ('y', '=i4'), ('z', '=i4')])
    with open(temp_subvolume_path, 'rb') as f:
        f.seek(0, 2)
        size = f.tell()
        f.seek(0, 0)  
        count = int(size / (4*4))
        data = np.fromfile(file = f, dtype = dt)
        
    f.close()
    
    return data['R'],np.concatenate((data['z'], data['y'], data['x']), axis=0).reshape(count, 3, order='F')


def getSubVolumesInfo(dims_orig,dims_target):
    """
    Returns the pixel coordinates that define the boundaries
    of all subvolumes.
    """
    print("...obtaining subvolumes information...")

    # Z-coordinate    
    n_z = np.int ( dims_orig[0] / dims_target[0])
    n_z_rem = dims_orig[0] % dims_target[0]
    
    n_z = np.arange(n_z+1)*dims_target[0]
    if n_z_rem > 0:
        n_z = np.append(n_z, n_z[-1]+n_z_rem)
    
    # Y-coordinate    
    n_y = np.int ( dims_orig[1] / dims_target[1])
    n_y_rem = dims_orig[1] % dims_target[1]
    
    n_y = np.arange(n_y+1)*dims_target[1]
    if n_y_rem > 0:
        n_y = np.append(n_y, n_y[-1]+n_y_rem)
    
    # X-coordinate    
    n_x = np.int ( dims_orig[2] / dims_target[2])
    n_x_rem = dims_orig[2] % dims_target[2]
    
    n_x = np.arange(n_x+1)*dims_target[2]
    if n_x_rem > 0:
        n_x = np.append(n_x, n_x[-1]+n_x_rem)

    return n_z, n_y, n_x


def calculateThickmapPerSubvolume(n_z, n_y, n_x, absFilename_template):
    """
    Loads each subvolume, calculates the thicknessmap and saves
    the result into a new RAW file.
    """
    print("...starting thickmap calculation for each volume...")
    N_subvol = ( len(n_z)-1 ) * ( len(n_y)-1 ) * ( len(n_x)-1 )
    
    Nvol = 0
    for zlim in range(0, len(n_z)-1 ):
        zrange_min = n_z[zlim]
        zrange_max = n_z[zlim+1]
        for ylim in range(0, len(n_y)-1 ):
            yrange_min = n_y[ylim]
            yrange_max = n_y[ylim+1]
            for xlim in range(0, len(n_x)-1 ):
                
                print("...Calculating Thickmap " + str(Nvol+1) + " of " + str(N_subvol))
                
#                 if Nvol < 13:
#                     #print(str(zlim))
#                     Nvol = Nvol + 1
#                     continue
                
                # (1) Get dimenstions
                xrange_min = n_x[xlim]
                xrange_max = n_x[xlim+1]
                dims = [zrange_max-zrange_min,yrange_max-yrange_min,xrange_max-xrange_min]
                
                # (2) Loading ridgepoints from subvolume
                ridge_points, zyx_coord = loadDistanceRidgeSubvolume(absFilename_template,Nvol)
                
                #(3) Ordering ridgepoints from subvolume
                ridge_points, zyx_coord = OrderDistanceRidgePoints(ridge_points, zyx_coord)
                
                #(4) Initilizing result and calulating thickness map from subvolume
                result = np.zeros(dims, dtype=np.float32)
                print("...starting thickmap calculation...")
                thickmap(dims, np.int32(zyx_coord[:,2]), np.int32(zyx_coord[:,1]), np.int32(zyx_coord[:,0]), np.float32(ridge_points), result)
                
                #(4) Save thickness map from subvolume
                print("...writing thickmap subvolume...")
                writeData3D(absFilename_template + "LT_" + str(Nvol).zfill(2)+".raw",result)
            
                Nvol = Nvol + 1
                
                
def connectThickMapSubvolumes(dims_orig, n_z, n_y, n_x, absFilename_template,TM_big_target):
    """
    Loads each subvolume, calculates the thicknessmap and saves
    the result into a new RAW file.
    """
    print("...connecting all subvolumes into a big volume...")
    
    # (1) Create memmap for large target volume
#    DR_big_array = np.memmap(TM_big_target,dtype=np.float32,mode='write',shape=(dims_orig[2],dims_orig[1],dims_orig[0]),order='C')
    DR_big_array = np.memmap(TM_big_target,dtype=np.float32,mode='write',shape=(dims_orig[0],dims_orig[1],dims_orig[2]),order='C') # change [lena]

    kk = 0
    for zlim in range(0, len(n_z)-1 ):
        zrange_min = n_z[zlim]
        zrange_max = n_z[zlim+1]
        for ylim in range(0, len(n_y)-1 ):
            yrange_min = n_y[ylim]
            yrange_max = n_y[ylim+1]
            for xlim in range(0, len(n_x)-1 ):
                # (1) Construct temporary subvolume path
                temp_subvolume_path = absFilename_template + str(kk).zfill(2)+".raw"
                
                # (2) Get temporary subvolume shapes
                xrange_min = n_x[xlim]
                xrange_max = n_x[xlim+1]
                
                # (3) Create temporary subvolume memory map for reading
                vol_temp = np.memmap(temp_subvolume_path,dtype=np.float32,mode='readonly',shape=(zrange_max-zrange_min,yrange_max-yrange_min,xrange_max-xrange_min),order='C')
                kk = kk + 1
                
                # (4) Fill subvolume into big volume
                print("...filling volume: " + str(kk).zfill(2) +"...")
                DR_big_array[zrange_min:zrange_max,yrange_min:yrange_max,xrange_min:xrange_max] = vol_temp


if __name__ == "__main__":
    """
    (0) Define constants
    """
    # NOTE: Order of dimensions is z, y, x
    #dims_orig   = [2160,2534,6327]
    #dims_target = [1024,1024,1024]
#    dims_orig   = [256,256,256]
#    dims_target = [107,150,156]
#    dims_orig   = [100,256,150]
#    dims_target = [100,256,150]
    
#    Filename_template = 'DRtemp_'
#    DR_big_source = '/home/lovric_g/Desktop/playground/02_EDT_DR_256.raw'
#    TM_big_target = '/home/lovric_g/Desktop/playground/DR_BIG_LT.raw'  # all temporary files will also be created here

    Filename_template = 'DRtemp_'
    DR_big_source = 'segm_px_sharpdiff_EDT_150x256x100.raw'
    TM_big_target = 'segm_px_sharpdiff_EDT_LT_big_150x256x100.raw'  # all temporary files will also be created here
    dims_orig   = [100,256,150]
    dims_target = [100,256,150]

#    Filename_template = 'DRtemp_'
#    DR_big_source = 'segm_px_distacemap_subsel_slices000-010_6900x6900x10.raw'
#    TM_big_target = 'segm_px_distacemap_subsel_slices000-010_LTmulti_6900x6900x10.raw'  # all temporary files will also be created here
#    dims_orig   = [10,6900,6900]
#    dims_target = [10,6900,6900]


    is_big_endian = True
    t_start = time.time()
    
    """
    (1) Get sizes of subvolumes and define working directory
    """
    n_z, n_y, n_x = getSubVolumesInfo(dims_orig,dims_target)
    workdir = os.path.dirname(TM_big_target)
    
    """
    (2) Load big distance ridge and separate 
    """
    loadDistanceRidgeAndSeparate(DR_big_source,workdir,Filename_template,dims_orig,dims_target,is_big_endian)
    print("--- Subvolume division: %s seconds ---" % (time.time() - t_start))
    
    """
    (3) Calculate Thickness map on each subvolume 
    """
    absFilename_template = os.path.join(workdir,Filename_template)
    calculateThickmapPerSubvolume(n_z, n_y, n_x, absFilename_template)

    """
    (4) Connect all subvolumes into a big volume 
    """
    absFilename_template = os.path.join(workdir,Filename_template+"LT_")

    connectThickMapSubvolumes(dims_orig, n_z, n_y, n_x, absFilename_template,TM_big_target)

    
#    runPseudoUnitTest(TM_big_target, "/home/lovric_g/Desktop/playground/03_EDT_DR_LT_256.raw", dims_orig)
#    runPseudoUnitTest(TM_big_target, "segm_px_sharpdiff_EDT_LT_150x256x100.raw", dims_orig)
    
