# module add psi-python36/4.4.0; module add gcc/7.3.0

import numpy as np
import time
from _thickmapopt import *


def readChunkedData3DandExtractRidgePoints(img_path, dims):
    """
    directly read ridge points from disk
    """
    chunksize = np.int(340000000)  # ~ 1GB chunk size

    d = dims[0]  # changed [lena]
    h = dims[1]  # TODO
    w = dims[2]  # change [lena]
    total_dim = np.int(w * h * d)

    n_chunks = np.int(total_dim / chunksize)
    chunk_rest = (w * h * d) % chunksize
    
    if chunksize > total_dim:
        chunksize = total_dim
    # added line tmp
    print('Chunk size: ', chunksize)

    with open(img_path, 'rb') as f:
        """
        (1) Initialize arrays
        """
        zyx_coord = np.zeros((chunksize, 3), dtype=np.uint16, order='C')
        ridge_points = np.zeros(chunksize, dtype=np.float32, order='C')

        zyx_coord_tmp = np.zeros((chunksize, 3), dtype=np.uint16, order='C')
        ridge_points_tmp = np.zeros(chunksize, dtype=np.float32, order='C')

        """
        (2) Load initial array
        """
        img_chunk = np.fromfile(f, dtype=np.dtype(">f4"), count=chunksize)

        ll = np.arange(0, len(img_chunk))
        ii = ll % w
#        jj = (ll / h) % (w)
#        kk = ll / (w * d)
        jj = (ll / w) % (h)  # lena's change
        kk = ll / (w * h)   # lena's change
       
        zyx_coord[:, 0] = kk
        zyx_coord[:, 1] = jj
        zyx_coord[:, 2] = ii

        ridge_points[:] = img_chunk[:]

        nonzero = ridge_points.flatten() > 0.0
        zyx_coord = zyx_coord[nonzero]
        ridge_points = ridge_points[nonzero]
        
        if chunksize == total_dim:
            f.close()
            return ridge_points, zyx_coord

        """
        (3) Loop through file chunks
        """
        for xll in range(1, n_chunks):
            img_chunk = np.fromfile(f, dtype=np.dtype(">f4"), count=chunksize)
            ll = ll + chunksize
            ii = ll % w
#            jj = (ll / h) % (w)
#            kk = ll / (w * d)
            jj = (ll / w) % (h)  # lena's change
            kk = ll / (w * h)   # lena's change
            zyx_coord_tmp[:, 0] = kk
            zyx_coord_tmp[:, 1] = jj
            zyx_coord_tmp[:, 2] = ii

            ridge_points_tmp[:] = img_chunk[:]

            nonzero = ridge_points_tmp.flatten() > 0.0
            ridge_points = np.concatenate(
                (ridge_points, ridge_points_tmp[nonzero]), axis=0)
            zyx_coord = np.concatenate(
                (zyx_coord, zyx_coord_tmp[nonzero]), axis=0)

        """
        (4) Loop through remainder
        """
        if chunk_rest > 0:
            ll = ll[-1] + 1 + np.arange(0, chunk_rest)
            img_chunk = np.fromfile(f, dtype=np.dtype(">f4"), count=chunk_rest)
            ii = ll % w
#            jj = (ll / h) % (w)
#            kk = ll / (w * d)
            jj = (ll / w) % (h)  # lena's change
            kk = ll / (w * h)   # lena's change
            zyx_coord_tmp[0:chunk_rest, 0] = kk
            zyx_coord_tmp[0:chunk_rest, 1] = jj
            zyx_coord_tmp[0:chunk_rest, 2] = ii
            
            ridge_points_tmp[0:chunk_rest] = img_chunk[0:chunk_rest]

            nonzero = ridge_points_tmp.flatten() > 0.0
            ridge_points = np.concatenate(
                (ridge_points, ridge_points_tmp[nonzero]), axis=0)
            zyx_coord = np.concatenate(
                (zyx_coord, zyx_coord_tmp[nonzero]), axis=0)

        f.close()
        return ridge_points, zyx_coord


def readData3D(img_path, dims):
    """
    read 3D RAW distance ridge data
    """
    print("...reading data...")

    d = np.int_(dims[0])  # w swaped with d [lena]
    h = np.int_(dims[1])  # TODO
    w = np.int_(dims[2])  # w swaped with d [lena]

    f = open(img_path,'rb')
    img_arr=np.fromfile(f,dtype=np.dtype(">f4"))
    #img_arr = np.memmap(img_path, dtype='float32', mode='r',shape=(d, h, w), order='C').byteswap()
    #img_arr=img_arr.byteswap()   # Adjust endianess

    img_arr=img_arr.reshape(d,h,w,order='C')

    f.close()
    return img_arr


def writeData3D(img_path, img_arr):
    """
    write 3D RAW data
    """
    print("...writing image data...")
    f = open(img_path,'w')
    img_arr.byteswap().tofile(f)
    f.close()


def getDistanceRidgePoints(img_arr):
    """
    Extract distance ridge points
    """
    print("...extracting ridge points...")
    nonzero = img_arr > 0.0  # where ridge is non-zero

    # (1) Load in an efficient manner
    # np.column_stack(np.where(nonzero))
    zyx_coord = np.int_(np.transpose(np.nonzero(img_arr)))
    ridge_points = np.float32(img_arr[nonzero])

    return ridge_points, zyx_coord


def OrderDistanceRidgePoints(ridge_points, zyx_coord):
    """
    Sort distance ridge points with biggest first
    """
    print("...ordering ridge points...")

    # use negative to have a reverse sorting
    sort_order = (-ridge_points).argsort()
    ridge_points = ridge_points[sort_order]
    zyx_coord = zyx_coord[sort_order]

    return ridge_points, zyx_coord


def runPseudoUnitTest(data_file, gt_data_file, dims):
    """
    compare Local thickness results with some given result from Fiji
    in order to have a Ground Truth
    """
    print("...making quick unit test...")
    image = readData3D(data_file,dims)
    image_gt = readData3D(gt_data_file,dims)
    
    print("Squared Diff (compare with Fiji):" + str( np.sum( np.abs( np.subtract(image, image_gt) ) ) ))
        

if __name__ == "__main__":
    """
    Runs the full chain of thickness map calculation
    """
    # NOTE: Order of dimensions is z, y, x

### lena test 1 equal dimention cube
#    dims = [256, 256, 256]
#    ridge_file = 'segm_px_sharpdiff_EDT_256x256x256.raw'
#    out_file = 'segm_px_sharpdiff_EDT_PyLT_256x256x256.raw'
#    gt_file = 'segm_px_sharpdiff_EDT_LT_256x256x256.raw'

### lena test 2 cube; slice number is different
#    dims = [100, 256, 256]
    
#    ridge_file = 'segm_px_sharpdiff_EDT_256x256x100.raw'
#    out_file = 'segm_px_sharpdiff_EDT_PyLT_dimswap_256x256x100.raw'
#    gt_file = 'segm_px_sharpdiff_EDT_LT_256x256x100.raw'
    
### lena test 3 cube; all dimentions are different
    dims = [100, 256, 150]
    
    ridge_file = 'segm_px_sharpdiff_EDT_150x256x100.raw'
    out_file = 'segm_px_sharpdiff_EDT_PyLT_dimswap_150x256x100.raw'
    gt_file = 'segm_px_sharpdiff_EDT_LT_150x256x100.raw'

    t_start = time.time()
    t_start_overall = t_start
#     img_arr = readData3D('/home/lovric_g/Desktop/playground/02_EDT_DR_1024.raw',dims)
#     print("--- Reading Data: %s seconds ---" % (time.time() - t_start))
# 
#     t_start = time.time()
#     ridge_points, zyx_coord = getDistanceRidgePoints(img_arr)
#     print("--- Extracting Distance ridge Data: %s seconds ---" % (time.time() - t_start))
# 
#     img_arr = 0  # FREE memory
#     t_start = time.time()
    ridge_points, zyx_coord = readChunkedData3DandExtractRidgePoints(ridge_file, dims)
    ridge_points, zyx_coord = OrderDistanceRidgePoints(ridge_points, zyx_coord)
    print("--- Ordering Data: %s seconds ---" % (time.time() - t_start))

    t_start = time.time()
    print("...starting thickmap calculation...")
    result = np.zeros(dims, dtype=np.float32)
    """
    This is possible as well, but does not allow multi-processing!!
    result = np.memmap('/home/lovric_g/Desktop/playground/mini_test_DR_PyLT_wrongEndian.raw',dtype=np.float32,mode='w+',shape=(dims[0],dims[1],dims[2]),order='C')
    """
    thickmap(dims,np.int32(zyx_coord[:,2]),np.int32(zyx_coord[:,1]),np.int32(zyx_coord[:,0]),ridge_points,result)
    img_arr = np.reshape(result, dims, order='C')
    print("--- Thickmap Calculation: %s seconds ---" % (time.time() - t_start))
     
    writeData3D(out_file,result)
    #writeData3D('/home/lovric_g/Desktop/playground/mini_test_DR_PyLT.raw',img_arr)
    runPseudoUnitTest(gt_file,out_file,dims)
    print("...DONEEEEE...")
    print("--- TOTAL time: %s seconds ---" % (time.time() - t_start_overall))
